import { UseToastOptions, useToast as useChakraToast } from '@chakra-ui/react'

const useToast = () => {
  const toast = useChakraToast()

  const showToast = (
    props: Omit<UseToastOptions, 'variant' | 'duration'> & { forever: boolean }
  ) => {
    const {
      title,
      description,
      status = 'success',
      isClosable = true,
      position = 'top',
      forever,
      ...rest
    } = props

    toast({
      title,
      description,
      status,
      duration: forever ? null : 3000,
      isClosable,
      position,
      variant: 'toast',
      ...rest
    })
  }

  return { toast: showToast }
}

export default useToast
