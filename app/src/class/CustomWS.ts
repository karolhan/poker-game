import Swal from 'sweetalert2'
import { ICloseEvent, IMessageEvent, w3cwebsocket as WS } from 'websocket'

export class CustomWS extends WS {
  onopenFunc: () => void = () => { }
  onopen = () => {
    console.log('WebSocket 連線已建立。')
    this.onopenFunc()
  }

  onmessageFunc: (data: object) => void = () => { }
  onmessage = (event: IMessageEvent) => {
    console.log('收到 WebSocket 消息。', event)
    if (JSON.parse(event.data as string)['message']) {
      Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: 'Websocket回傳異常',
        text: event.data as string,
        showConfirmButton: false,
      })
    } else {
      this.onmessageFunc(JSON.parse(event.data as string))
    }
  }

  // oncloseFunc: (event: ICloseEvent) => void = () => { }
  onclose = (event: ICloseEvent) => {
    console.log('WebSocket 連線已關閉。', event)
  }

  // onerrorFunc: (error: Error) => void = () => { }
  onerror = (error: Error) => {
    console.error('WebSocket 錯誤發生。', error)
    // Swal.fire({
    //   position: 'top-end',
    //   icon: 'error',
    //   title: 'Websocket錯誤發生',
    //   text: JSON.stringify(error),
    //   showConfirmButton: false,
    // })
  }
}