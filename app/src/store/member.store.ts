import { create } from 'zustand'

interface MemberState {
  member: Member | null
  // memberList: Member[]
  roomId: string
  setMember: (value: Member) => void
  clearMember: () => void
  // setMemberList: (value: Member[]) => void
  setRoomId: (value: string) => void
}


export const useMemberStore = create<MemberState>((set, get) => ({
  member: null,
  // memberList: [],
  roomId: '',
  setMember: (value: Member) => {
    set({ member: value })
  },
  clearMember: () => {
    set({ member: null })
  },
  // setMemberList: (value: Member[]) => {
  //   set({ memberList: value })
  // },
  setRoomId: (value: string) => {
    set({ roomId: value })
  },
}))
