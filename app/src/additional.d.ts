type View = 'index' | 'lobby' | 'room'

interface Member {
  id: string
  name: string
  connect_time: number
  money: number
  roomId?: string
}

interface MemberOnList {
  name: string
  money: number
}