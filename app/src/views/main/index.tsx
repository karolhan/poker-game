import { GlobalContext } from '../../views/App'
import { Input } from '@chakra-ui/react'
import { Button } from '@chakra-ui/react'
import { useState, useContext } from 'react'
import { useWebSocket } from '../../components/WebSocketProvider'
import React from 'react'

export default function Index(props: { setView: (view: View) => void }) {
  const { setView } = props
  const [name, setName] = useState<string>('')

  const { swal: SWAL } = useContext(GlobalContext)

  const socket = useWebSocket()

  const onSubmit = () => {
    if (!name.trim()) {
      SWAL.fire({
        icon: 'warning',
        title: '請輸入名稱~',
      })
    } else {
      setView('lobby')
      socket.send(
        JSON.stringify({
          action: 'setName',
          member_name: name,
        }),
      )
    }
  }
  return (
    <div className="bg-[url('/public/img/bg_index.jpg')] bg-contain h-screen p-5 grid place-content-around">
      <h1 className="text-[50px] desktop:text-[90px] font-black text-white ml-5 mt-3">STUD POKER</h1>
      <div className="flex justify-center gap-2">
        <Input
          placeholder="Enter Your Name"
          colorScheme="blue"
          className="!bg-blue-100 !w-2/3"
          onChange={(e) => setName(e.target.value)}
        />
        <Button colorScheme="yellow" onClick={onSubmit}>
          START GAME
        </Button>
      </div>
    </div>
  )
}
