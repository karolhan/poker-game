import { useWebSocket } from '../../components/WebSocketProvider'
import { GlobalContext } from '../../views/App'
import { useContext, useState } from 'react'
import ConnectListDrawer from '../../components/ConnectListDrawer'
import { useMemberStore } from '../../store/member.store'
import React from 'react'
import { Button } from '@chakra-ui/react'

// const anteList = [10, 50, 100, 500]

export default function Lobby(props: { setView: (view: View) => void }) {
  const { setView } = props

  const [memberList, setMemberList] = useState<MemberOnList[]>([])

  const member = useMemberStore((state) => state.member)
  const setMember = useMemberStore((state) => state.setMember)

  const { swal: SWAL } = useContext(GlobalContext)

  const socket = useWebSocket()

  socket.onmessageFunc = (data: any) => {
    const { connect_member_name, disconnect_member_name, members, me } = data
    setMemberList(members)
    let title
    if (connect_member_name) title = `[${connect_member_name}]成功進入大廳`
    if (disconnect_member_name) title = `[${disconnect_member_name}]已離線`
    if (me) setMember(me)
    SWAL.fire({
      position: 'bottom-end',
      icon: 'success',
      title,
      timer: 2000,
      showConfirmButton: false,
    })
  }

  const autoStartOnClick = () => {
    setView('room')
    socket.send(
      JSON.stringify({
        action: 'autoStart',
      }),
    )
  }

  return (
    <div className="bg-[url('/public/img/bg_lobby.jpg')] bg-cover h-screen p-5">
      <ConnectListDrawer members={memberList} member={member} />
      <h1 className="text-[40px] desktop:text-[80px] font-black text-white ml-5 mt-10 mb-5 text-center">
        LOBBY 自動配桌
      </h1>
      <div className="text-center">
        <Button colorScheme="facebook" onClick={autoStartOnClick}>
          開始配桌(底注10元)
        </Button>
      </div>
      {/* <div className="grid grid-cols-2 gap-2 desktop:p-5">
        {anteList.map((ante, i) => (
          <div
            className="grid place-content-around relative cursor-pointer"
            key={i}
            onClick={() => autoStartOnClick(ante)}
          >
            <img src="/img/lobby_room.png" alt="" className="h-[100px] w-[200px] desktop:h-[200px] desktop:w-[350px]" />
            <div className="absolute inset-0 flex items-center justify-center text-white font-bold text-xl desktop:text-5xl">
              底注{ante}
            </div>
          </div>
        ))}
      </div> */}
    </div>
  )
}
