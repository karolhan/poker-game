import React, { useContext, useState } from 'react'
import { GlobalContext } from '../App'
import { useWebSocket } from '../../components/WebSocketProvider'
import { Button } from '@chakra-ui/react'
import { useMemberStore } from '../../store/member.store'
import PokerTable from '../../components/PokerTable'

export default function Room(props: { setView: (view: View) => void }) {
  const { setView } = props

  const member = useMemberStore((state) => state.member)

  // const [isStart, setIsStart] = useState<boolean>(false)
  const [cardInfo, setCardInfo] = useState()
  const [speakerId, setSpeakerId] = useState<string>()
  const [opponentId, setOpponentId] = useState<string>('')

  const { swal: SWAL } = useContext(GlobalContext)

  const socket = useWebSocket()

  socket.onmessageFunc = (data: any) => {
    console.log('data', data)
    const { card_info, speaker_id } = data
    if (card_info && speaker_id) {
      const opponentId = Object.keys(card_info).find((key) => key !== member?.id) as string
      setOpponentId(opponentId)
      setCardInfo(card_info)
      setSpeakerId(speaker_id)
    }

    if (speaker_id === member?.id) {
    }
  }

  return (
    <div className="bg-[url('/public/img/bg_room.jpg')] bg-cover h-screen p-10 grid relative">
      <div className="absolute top-5 right-5">
        <Button colorScheme="orange" onClick={() => setView('lobby')}>
          回大廳
        </Button>
      </div>

      {cardInfo ? (
        <div className="flex flex-col gap-3 mt-10">
          <PokerTable cards={cardInfo[opponentId]} />
          <div className="bg-white rounded border p-1 flex justify-center">第？輪 下注者: {speakerId}</div>
          <PokerTable cards={cardInfo[member!.id]} />
          <div className="flex gap-2">
            <Button colorScheme="yellow">RAISE</Button>
            <Button colorScheme="green">CALL</Button>
            <Button colorScheme="red">DROP</Button>
          </div>
        </div>
      ) : (
        <h1>等待玩家加入中~~~</h1>
      )}
    </div>
  )
}
