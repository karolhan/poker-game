import { ChakraProvider } from '@chakra-ui/react'
import { useState, createContext, useEffect } from 'react'
import Index from './main/index'
import Lobby from './main/lobby'
import Room from './main/room'
import Swal from 'sweetalert2'
import withReactContent, { ReactSweetAlert } from 'sweetalert2-react-content'
import { WebSocketProvider } from '../components/WebSocketProvider'
import { useMemberStore } from '../store/member.store'

interface GlobalContextType {
  swal: ReactSweetAlert
}

export const GlobalContext = createContext<GlobalContextType>({} as GlobalContextType)

function App() {
  const [view, setView] = useState<View>('index')

  const member = useMemberStore((state) => state.member)

  useEffect(() => {
    console.log('member', member)
    if (member) {
      setView('lobby')
    }
  }, [])

  return (
    <ChakraProvider>
      <WebSocketProvider>
        <GlobalContext.Provider
          value={{
            swal: withReactContent(Swal),
          }}
        >
          {view === 'index' && <Index setView={setView} />}
          {view === 'lobby' && <Lobby setView={setView} />}
          {view === 'room' && <Room setView={setView} />}
        </GlobalContext.Provider>
      </WebSocketProvider>
    </ChakraProvider>
  )
}

export default App
