import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  Button,
  useDisclosure,
  List,
  ListItem,
  Divider,
} from '@chakra-ui/react'

export default function ConnectListDrawer({ members, member }: { members: MemberOnList[]; member: Member | null }) {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <Button colorScheme="teal" size="sm" className="!absolute top-5 right-5" onClick={onOpen}>
        線上名單
      </Button>
      <Drawer onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader borderBottomWidth="1px">線上玩家</DrawerHeader>
          <DrawerBody>
            <List spacing={3}>
              {members.map((member, i) => (
                <div key={i}>
                  <ListItem>
                    <div className="flex justify-between">
                      <span>{member.name}</span>
                      <span>${member.money}</span>
                    </div>
                  </ListItem>
                  <Divider />
                </div>
              ))}
            </List>
          </DrawerBody>
          {member && (
            <DrawerFooter className="flex-col border-t-2 bg-blue-300">
              <div>
                Player ID: <strong>{member.id}</strong>
              </div>
              <div>
                Player Name: <strong>{member.name}</strong>
              </div>
            </DrawerFooter>
          )}
        </DrawerContent>
      </Drawer>
    </>
  )
}
