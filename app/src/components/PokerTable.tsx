import { CARD_SUIT } from '../constants'

export default function PokerTable({ cards }: { cards: number[][] }) {
  return (
    <div className="grid grid-cols-5 gap-1">
      {(cards as number[][]).map((card: number[]) => (
        <div className="bg-[url('/public/img/card_bg.jpg')] bg-cover text-slate-900 font-bold h-48 p-1 text-2xl">
          {card && CARD_SUIT[card[0] - 1] + card[1]}
        </div>
      ))}
    </div>
  )
}
