import React, { createContext, useContext, useEffect, useState } from 'react'
import { CustomWS } from '../class/CustomWS'

const WebSocketContext = createContext({} as CustomWS)

export const WebSocketProvider = ({ children }: { children: React.ReactNode }) => {
  const [socket, setSocket] = useState<CustomWS>({} as CustomWS)

  useEffect(() => {
    const newSocket = new CustomWS('wss://hkmqybqx6c.execute-api.ap-southeast-1.amazonaws.com/production')
    setSocket(newSocket)

    return () => {
      newSocket.close()
    }
  }, [])

  return <WebSocketContext.Provider value={socket}>{children}</WebSocketContext.Provider>
}

export const useWebSocket = () => {
  return useContext(WebSocketContext)
}
