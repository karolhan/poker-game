/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/views/App.tsx', './src/views/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      screens: {
        desktop: '1024px',
      },
    },
  },
  plugins: [],
}
